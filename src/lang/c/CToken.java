package lang.c;

import lang.SimpleToken;

public class CToken extends SimpleToken {
	public static final int TK_PLUS			= 2;				// +
	public static final int TK_MINUS		= 3;				// -
	public static final int TK_AMPERSAND	= 4;				// &(アドレス)
	public static final int TK_OCTAL		= 5;	// 8進数
	public static final int TK_DECIMAL		= 6;	// 10進数
	public static final int TK_HEXADECIMAL	= 7;	// 16進数






	public CToken(int type, int lineNo, int colNo, String s) {
		super(type, lineNo, colNo, s);
	}
}
