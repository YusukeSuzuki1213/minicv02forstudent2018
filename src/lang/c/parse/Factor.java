package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Factor extends CParseRule {
	// factor ::= factorAmp | factor
	private CParseRule node;
	public Factor(CParseContext pcx) {
	}
	public static boolean isFirst(CToken tk) {
		return FactorAmp.isFirst(tk) || Number.isFirst(tk) ;
	}
	public void parse(CParseContext pcx) throws FatalErrorException {
		// ここにやってくるときは、必ずisFirst()が満たされている

		CTokenizer ct = pcx.getTokenizer();
		CToken tk = ct.getCurrentToken(pcx);

		if (FactorAmp.isFirst(tk)) {
			node = new FactorAmp(pcx);
		} else if (Number.isFirst(tk)){
			node = new Number(pcx);
		} else {
			pcx.fatalError("Factorクラスでエラー");
		}
		node.parse(pcx);
	}

	public void semanticCheck(CParseContext pcx) throws FatalErrorException {
		if (node != null) {
			node.semanticCheck(pcx);
			setCType(node.getCType());		// node の型をそのままコピー
			setConstant(node.isConstant());
		}
	}

	public void codeGen(CParseContext pcx) throws FatalErrorException {
		PrintStream o = pcx.getIOContext().getOutStream();
		o.println(";;; factor starts");
		if (node != null) { node.codeGen(pcx); }
		o.println(";;; factor completes");
	}
}